import 'package:flutter/material.dart';

class UserProfile extends StatelessWidget {
  const UserProfile({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: const Text("User Profile")),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            const CircleAvatar(
              backgroundImage: AssetImage("login.png"),
            ),
            const SizedBox(height: 40),
            Padding(
              padding: const EdgeInsets.symmetric(horizontal: 80),
              child: TextFormField(
                keyboardType: TextInputType.name,
                onChanged: (String value) {},
                decoration: const InputDecoration(
                    border: OutlineInputBorder(), hintText: "Username"),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
